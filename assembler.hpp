#include <unordered_map>
#include <vector>
#include <cstring>
#include "main.h"
using namespace std;
int *cp;
int lbidx[100000], lblex[100000], lbcnt;
char numstr[][5] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"};
char regstr[][10] = {"zero", "at", "v0", "v1", "a0", "a1", "a2", "a3", "t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7", "t8", "t9", "s0", "s1", "s2", "s3", "s4", "s5", "s6", "s7", "k0", "k1", "gp", "sp", "s8", "ra", "lo", "hi", "pc"};
char assstr[][10] = {"align", "ascii", "asciiz", "byte", "half", "word", "space", "data", "text"};
char insstr[][10] = {"add", "addu", "addiu", "sub", "subu", "mul", "mulu", "div", "divu", "xor", "xoru", "neg", "negu", "rem", "remu", "li", "seq", "sge", "sgt", "sle", "slt", "sne", "move", "mfhi", "mflo", "la", "lb", "lh", "lw", "sb", "sh", "sw", "b", "beq", "bne", "bge", "ble", "bgt", "blt", "beqz", "bnez", "blez", "bgez", "bgtz", "bltz", "j", "jr", "jal", "jalr", "nop", "syscall"};
class MIPSAssembler
{
	enum tid
	{
	    asst, opct, regt, numt, litt, addt, labt, lbdt
	};
	struct token
	{
		tid type;
		int nval;
		string sval;
		token(): type(asst), nval(0), sval() {}
		token(tid _t, int _n): type(_t), nval(_n), sval() {}
	};
	unordered_map<string, byte> opmap;
	unordered_map<string, int> lbmap;
	vector<token> lex;

	token getlit(string &str, int &p)
	{
		token ret;
		ret.type = litt;
		string &v = ret.sval;
		int len = str.length(), f = 1;
		while(p < len && f)
		{
			switch (str[p])
			{
				case '\"':
					f = 0;
					break;
				case '\\':
				{
					switch(str[++p])
					{
						case 'n':
							v += '\n';
							break;
						case 't':
							v += '\t';
							break;
						case '\\':
						case '\'':
						case '\"':
							v += str[p];
							break;
						default:
							exit(1);
					}
					break;
				}
				default:
					v += str[p];
			}
			++p;
		}
		return ret;
	}
	token gettxt(string &str, int &p, tid typ)
	{
		token ret;
		ret.type = typ;
		string &v = ret.sval;
		int len = str.length(), f = 1;
		while(p < len && f)
		{
			switch (str[p])
			{
				case ' ':
				case '\t':
				case '\n':
				case ',':
					f = 0;
					break;
				case '\"':
					f = 0;
					break;
				case '\\':
				{
					switch(str[++p])
					{
						case 'n':
							v += '\n';
							break;
						case 't':
							v += '\t';
							break;
						case '\\':
						case '\'':
						case '\"':
							v += str[p];
							break;
						default:
							exit(1);
					}
					break;
				}
				case ':':
					f = 0;
					ret.type = lbdt;
					break;
				default:
					v += str[p];
			}
			++p;
		}
		if (typ == opct && ret.type != lbdt && !opmap[v]) ret.type = labt;
		return ret;
	}
	token getnum(string &str, int &p)
	{
		token ret;
		ret.type = numt;
		int s = 1, len = str.length(), &v = ret.nval;
		if (str[p] == '-') s = -1, ++p;
		int t = p;
		while (p < len && str[p] >= '0' && str[p] <= '9') ++p;
		for (int i = p - 1, j = 1; i >= t; --i, j *= 10)
		{
			v += j * (str[i] - '0');
		}
		v *= s;
		if (str[p] == '(')
		{
			if (str[++p] != '$')
			{
				return ret;
			}
			ret.type = addt;
			while (str[++p] != ')')
			{
				ret.sval += str[p];
			}
			++p;
		}
		return ret;
	}
	void appexpr(string &str)
	{
		int p = 0, len = str.length();
		while(p < len)
		{
			switch(str[p])
			{
				case ' ':
				case '\t':
				case ',':
					++p;
					break;
				case '.':
					lex.push_back(gettxt(str, ++p, asst));
					break;
				case '-':
				case '+':
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					lex.push_back(getnum(str, p));
					break;
				case '$':
					lex.push_back(gettxt(str, ++p, regt));
					break;
				case '\"':
					lex.push_back(getlit(str, ++p));
					break;
				case '\'':
					lex.push_back(token(numt, (int)str[++p]));
					p += 2;
					break;
				case '#':
					return;
				default:
					lex.push_back(gettxt(str, p, opct));
			}
		}
	}
	void eva(unsigned int &p)
	{
		if (p > lex.size()) return;
		switch (lex[p].type)
		{
			case asst:
			{
				switch(opmap[lex[p].sval])
				{
					case 1:   //.align
					{
						int n = lex[++p].nval;
						int t = 1 << n;
						*cp = ((*cp - 1) / t + 1) * t;
						break;
					}
					case 2:   //.ascii
					{
						string &v = lex[++p].sval;
						for (unsigned int i = 0; i < v.length(); ++i)
						{
							data[(*cp)++] = v[i];
						}
						break;
					}
					case 3:   //.asciiz
					{
						string &v = lex[++p].sval;
						for (unsigned int i = 0; i < v.length(); ++i)
						{
							data[(*cp)++] = v[i];
						}
						data[(*cp)++] = '\0';
						break;
					}
					case 4:   //.byte
					{
						while (lex[++p].type == numt)
						{
							data[(*cp)++] = (byte)lex[p].nval;
						}
						--p;
						break;
					}
					case 5:   //.half
					{
						do
						{
							memcpy(data + *cp, &lex[++p].nval, sizeof(half));
							*cp += sizeof(half);
						}
						while (lex[p + 1].type == numt);
						break;
					}
					case 6:   //.word
					{
						while (lex[++p].type == numt)
						{
							memcpy(data + *cp, &lex[p].nval, sizeof(word));
							*cp += sizeof(word);
						}
						--p;
						break;
					}
					case 7:   //.space
					{
						int n = lex[++p].nval;
						memset(data + *cp, 0, n * sizeof(byte));
						break;
					}
					case 8://.data
						cp = &data_p;
						break;
					case 9://.text
						cp = &text_p;
						break;
					default:
						exit(1);
				}
				++p;
				break;
			}
			case opct:
			{
				data[*cp] = opmap[lex[p].sval];
				int cnt = 0, rcnt = 0, f = 1;
				while (f)
				{
					switch (lex[++p].type)
					{
						case opct:
						case asst:
						case lbdt:
							f = 0;
							break;
						case numt:
						{
							data[*cp + 2 + cnt * 5] = 1;
							memcpy(data + *cp + 3 + cnt * 5, &lex[p].nval, sizeof(word));
							++cnt;
							break;
						}
						case regt:
						{
							if (rcnt)
							{
								data[*cp + 2 + cnt * 5] = 2;
								data[*cp + 3 + cnt * 5] = opmap[lex[p].sval];
								++cnt;
							}
							else
							{
								data[*cp + 1] = opmap[lex[p].sval];
								++rcnt;
							}
							break;
						}
						case labt:
						{
							if (lbmap[lex[p].sval])
							{
								memcpy(data + *cp + 8, &lbmap[lex[p].sval], sizeof(word));
							}
							else
							{
								lbidx[lbcnt] = *cp;
								lblex[lbcnt++] = p;
							}
							break;
						}
						case addt:
						{
							data[*cp + 7] = 1;
							memcpy(data + *cp + 8, &lex[p].nval, sizeof(word));
							data[*cp + 2] = 2;
							data[*cp + 3] = opmap[lex[p].sval];
							break;
						}
						default:
							exit(1);
					}
				}
				(*cp) += 12;
				break;
			}
			case lbdt:
			{
				lbmap[lex[p++].sval] = *cp;
				break;
			}
			default:
				exit(1);
		}
	}
public:
	MIPSAssembler()
	{
		for (int i = 0; i < 32; ++i)
		{
			opmap[numstr[i]] = i;
		}
		for (int i = 0; i < 35; ++i)
		{
			opmap[regstr[i]] = i;
		}
		opmap["fp"] = 30;
		for (int i = 0; i < 9; ++i)
		{
			opmap[assstr[i]] = i + 1;
		}
		for (int i = 0; i < 51; ++i)
		{
			opmap[insstr[i]] = i + 1;
		}
	}
	int compile(ifstream & fas)
	{
		string str;
		while (!fas.eof())
		{
			getline(fas, str);
			appexpr(str);
		}
		unsigned int pos = 0, s = lex.size();
		while (pos < s)
		{
			eva(pos);
		}
		for (int i = 0; i < lbcnt; ++i)
		{
			memcpy(data + lbidx[i] + 8, &lbmap[lex[lblex[i]].sval], sizeof(word));
		}
		return lbmap["main"];
	}
};
