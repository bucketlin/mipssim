#ifndef _INST_H
#define _INST_H
#include <iostream>
#include "main.h"
#define GOTO(x) ret[1] = ret[0] = 1,ret[2] = 34, ret[3] = x;
void ADD(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = pd[1] + pd[2];
}
void ADDU(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = (unsigned)(pd[1] + pd[2]);
}
void ADDIU(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = (unsigned)(pd[1] + pd[2]);
}
void SUB(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = pd[1] - pd[2];
}
void SUBU(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = (unsigned)(pd[1] - pd[2]);
}
void MUL(int *ret, long long *pd)
{
	ret[0] = 1;
	if (pd[4])
	{
		ret[1] = 1;
		ret[2] = pd[3];
		ret[3] = pd[1] * pd[2];
	}
	else
	{
		ret[1] = 2;
		unsigned long long t = pd[0] * pd[1];
		ret[2] = 33;
		ret[3] = t >> 32;
		ret[4] = 32;
		ret[5] = t & ((1LL << 32) - 1);
	}
}
void MULU(int *ret, long long *pd)
{
	ret[0] = 1;
	if (pd[4])
	{
		ret[1] = 1;
		ret[2] = pd[3];
		ret[3] = (unsigned)(pd[1] * pd[2]);
	}
	else
	{
		unsigned long long t = pd[0] * pd[1];
		ret[1] = 2;
		ret[2] = 33;
		ret[3] = t >> 32;
		ret[4] = 32;
		ret[5] = t & ((1LL << 32) - 1);
	}
}
void DIVU(int *ret, long long *pd)
{
	ret[0] = 1;
	if (pd[4])
	{
		ret[1] = 1;
		ret[2] = pd[3];
		ret[3] = (unsigned)(pd[1] / pd[2]);
	}
	else
	{
		ret[1] = 2;
		ret[2] = 33;
		ret[3] = (unsigned)(pd[0] % pd[1]);
		ret[4] = 32;
		ret[5] = (unsigned)(pd[0] / pd[1]);
	}
}
void DIV(int *ret, long long *pd)
{
	ret[0] = 1;
	if (pd[4])
	{
		ret[1] = 1;
		ret[2] = pd[3];
		ret[3] = pd[1] / pd[2];
	}
	else
	{
		ret[1] = 2;
		ret[2] = 33;
		ret[3] = pd[0] % pd[1];
		ret[4] = 32;
		ret[5] = pd[0] / pd[1];
	}
}
void XOR(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = pd[1] ^ pd[2];
}
void XORU(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = (unsigned)(pd[1] ^ pd[2]);
}
void NEG(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = -pd[1];
}
void NEGU(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = (unsigned)(-pd[1]);
}
void REM(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = pd[1] % pd[2];
}
void REMU(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = (unsigned)(pd[1] % pd[2]);
}
void LI(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = pd[1];
}
void SEQ(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = (pd[1] == pd[2]);
}
void SGE(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = (pd[1] >= pd[2]);
}
void SGT(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = (pd[1] > pd[2]);
}
void SLE(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = (pd[1] <= pd[2]);
}
void SLT(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = (pd[1] < pd[2]);
}
void SNE(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = (pd[1] != pd[2]);
}
void B(int *ret, long long *pd)
{
	GOTO(pd[2]);
}
void BEQ(int *ret, long long *pd)
{
	if (pd[0] == pd[1])	GOTO(pd[2])
	}
void BNE(int *ret, long long *pd)
{
	if (pd[0] != pd[1])	GOTO(pd[2]);
}
void BGE(int *ret, long long *pd)
{
	if (pd[0] >= pd[1])	GOTO(pd[2]);
}
void BLE(int *ret, long long *pd)
{
	if (pd[0] <= pd[1])	GOTO(pd[2]);
}
void BGT(int *ret, long long *pd)
{
	if (pd[0] > pd[1])	GOTO(pd[2]);
}
void BLT(int *ret, long long *pd)
{
	if (pd[0] < pd[1])	GOTO(pd[2]);
}
void BEQZ(int *ret, long long *pd)
{
	if (pd[0] == 0)	GOTO(pd[2]);
}
void BNEZ(int *ret, long long *pd)
{
	if (pd[0] != 0)	GOTO(pd[2]);
}
void BGEZ(int *ret, long long *pd)
{
	if (pd[0] >= 0)	GOTO(pd[2]);
}
void BLEZ(int *ret, long long *pd)
{
	if (pd[0] <= 0)	GOTO(pd[2]);
}
void BGTZ(int *ret, long long *pd)
{
	if (pd[0] > 0) GOTO(pd[2]);
}
void BLTZ(int *ret, long long *pd)
{
	if (pd[0] < 0) GOTO(pd[2]);
}
void J(int *ret, long long *pd)
{
	GOTO(pd[2]);
}
void JR(int *ret, long long *pd)
{
	GOTO(pd[0]);
}
void JAL(int *ret, long long *pd)
{
	ret[0] = 1;
	ret[1] = 2;
	ret[2] = 31;
	ret[3] = pd[1];
	ret[4] = 34;
	ret[5] = pd[2];
}
void JALR(int *ret, long long *pd)
{
	ret[0] = 1;
	ret[1] = 2;
	ret[2] = 31;
	ret[3] = pd[1];
	ret[4] = 34;
	ret[5] = pd[0];
}
void LA(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = pd[2] + pd[1];
}
void LB(int *ret, long long *pd)
{
	ret[0] = 2;
	ret[2] = pd[2] + pd[1];
	ret[3] = pd[3];
	ret[4] = sizeof(byte);
}
void LH(int *ret, long long *pd)
{
	ret[0] = 2;
	ret[2] = pd[2] + pd[1];
	ret[3] = pd[3];
	ret[4] = sizeof(half);
}
void LW(int *ret, long long *pd)
{
	ret[0] = 2;
	ret[2] = pd[2] + pd[1];
	ret[3] = pd[3];
	ret[4] = sizeof(word);
}
void SB(int *ret, long long *pd)
{
	ret[0] = 3;
	ret[2] = pd[2] + pd[1];
	ret[3] = pd[0];
	ret[4] = sizeof(byte);
}
void SH(int *ret, long long *pd)
{
	ret[0] = 3;
	ret[2] = pd[2] + pd[1];
	ret[3] = pd[0];
	ret[4] = sizeof(half);
}
void SW(int *ret, long long *pd)
{
	ret[0] = 3;
	ret[2] = pd[2] + pd[1];
	ret[3] = pd[0];
	ret[4] = sizeof(word);
}
void MOVE(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = pd[1];
}
void MFHI(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = pd[1];
}
void MFLO(int *ret, long long *pd)
{
	ret[1] = ret[0] = 1;
	ret[2] = pd[3];
	ret[3] = pd[1];
}
void NOP(int *ret, long long *pd)
{
	ret[0] = pd[0] = 0;
}
void SYSCALL(int *ret, long long *pd)
{
	switch (pd[0])
	{
		case 1:
		{
			std::cout << pd[1];
			break;
		}
		case 4:
		{
			std::cout << (data + pd[1]);
			break;
		}
		case 5:
		{
			ret[1] = ret[0] = 1;
			ret[2] = 2;
			std::cin >> ret[3];
			break;
		}
		case 8:
		{
			std::cin >> (data + pd[1]);
			break;
		}
		case 9:
		{
			data_p = ((data_p - 1) / 4 + 1) * 4;
			ret[1] = ret[0] = 1;
			ret[2] = 2;
			ret[3] = data_p;
			data_p += pd[1];
			break;
		}
		case 10:
			ret[1] = ret[0] = 1;
			ret[2] = 34;
			ret[3] = 0;
			break;
		case 17:
			ret[1] = ret[0] = 1;
			ret[2] = 34;
			ret[3] = 0;
			break;
		default:
			exit(2);
	}
}
void (*insp[64])(int *ret, long long *pd) = {nullptr, ADD, ADDU, ADDIU, SUB, SUBU, MUL, MULU, DIV, DIVU, XOR, XORU, NEG, NEGU, REM, REMU, LI, SEQ, SGE, SGT, SLE, SLT, SNE, MOVE, MFHI, MFLO, LA, LB, LH, LW, SB, SH, SW, B, BEQ, BNE, BGE, BLE, BGT, BLT, BEQZ, BNEZ, BLEZ, BGEZ, BGTZ, BLTZ, J, JR, JAL, JALR, NOP, SYSCALL};
#endif
