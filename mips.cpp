#include <fstream>
#include "main.h"
#include "assembler.hpp"
#include "mipsim.hpp"
using namespace std;
int main(int argc, char* argv[])
{
//	freopen("myerr", "w", stderr);
	if (argc < 2)
	{
		cout << "no input file, exiting" << endl;
		return 0;
	}
	ifstream fas;
	fas.open(argv[1]);
	MIPSAssembler assm;
	int m = assm.compile(fas);
	MIPSIM sim(m);
	sim.run();
	return 0;
}

